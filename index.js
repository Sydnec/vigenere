import express from "express";

const app = express();
const port = 3000;

app.use(express.static("public"));
app.locals.basedir = "./public";
app.set("view engine", "pug");

//Routage
app.get("/", function (req, res) {
  res.render("index");
});

app.listen(port);
