function ciphering(str, key, sens = 1) {
  var output = "";
  var cpt = 0;
  key = key.replace(/ /g, "");
  for (let i = 0; i < str.length; i++) {
    var char = str[i];
    // Si le caractère est une lettre
    if (char.match(/[a-zA-Z]/i)) {
      // Code ascii associé a la lettre
      var charCode = char.charCodeAt();
      // Incrémentation correspondant a la lettre de la clé
      var keyCharInt = key[cpt % key.length].toUpperCase().charCodeAt() - 65;
      cpt++;
      //  Majuscules
      if (charCode >= 65 && charCode <= 90) {
        var newChar = String.fromCharCode(
          ((26 + charCode - 65 + sens * keyCharInt) % 26) + 65
        );
      }
      //  En cas de minuscules
      else if (charCode >= 97 && charCode <= 122) {
        var newChar = String.fromCharCode(
          ((26 + charCode - 97 + sens * keyCharInt) % 26) + 97
        );
      }
      output += newChar;
    } else {
      output += char;
    }
  }
  return output;
}

function deciphering(str, key) {
  return ciphering(str, key, -1);
}

export { ciphering, deciphering };
