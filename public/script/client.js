import { ciphering, deciphering } from "./vigenere.js";

const uncrypted = document.getElementById("uncryptedString");
const key = document.getElementById("crypteKey");
const crypted = document.getElementById("cryptedString");

document
  .getElementById("cipheringButton")
  .addEventListener("click", cipheringButton);
document
  .getElementById("decipheringButton")
  .addEventListener("click", decipheringButton);

function cipheringButton() {
  crypted.value = ciphering(uncrypted.value, key.value);
  uncrypted.value = "";
}

function decipheringButton() {
  uncrypted.value = deciphering(crypted.value, key.value);
  crypted.value = "";
}
