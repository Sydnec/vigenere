import { ciphering, deciphering } from "./vigenere.js";

test('Chiffrement de "Test de fonctionnement du chiffrement" par "CLE"', () => {
  expect(ciphering("Test de fonctionnement du chiffrement", "CLE")).toBe(
    "Vpwv oi hzreemqyrgxipe hw nlkqjtpqgyx"
  );
});
test('Dechiffrement de "Vpwv oi hzreemqyrgxipe hw oiesmhqvgxipe" par "CLE', () => {
  expect(deciphering("Vpwv oi hzreemqyrgxipe hw oiesmhqvgxipe", "CLE")).toBe(
    "Test de fonctionnement du dechiffrement"
  );
});
test('Chiffrement de "Salut aujourd hui intro au chiffrement de Vigenere" par "CLE DE VIGENERE"', () => {
  expect(
    ciphering(
      "Salut aujourd hui intro au chiffrement de Vigenere",
      "CLE DE VIGENERE"
    )
  ).toBe("Ulpxx vcpshvu lwt mqxmw gy plzjhcipiib ji Imxippvh");
});
test('Dechiffrement de "Ulpxx vcpshvu lwt mqxmw gy plzjhcipiib ji Imxippvh" par "CLE DE VIGENERE', () => {
  expect(
    deciphering(
      "Ulpxx vcpshvu lwt mqxmw gy plzjhcipiib ji Imxippvh",
      "CLE DE VIGENERE"
    )
  ).toBe("Salut aujourd hui intro au chiffrement de Vigenere");
});
